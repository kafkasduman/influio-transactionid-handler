# Influio | Gtm Transaction Id Handler Şablonu






## Açıklamalar

Influio GTM TransactionId Takip Şablonu, Influio entegrasyonunun google tag manager aracılığıyla tamamlanmasına yardımcı olur.
Bu şablon, Influio tarafından yönlendirilen trafikğin ayrıştırılması amacıyla kullanılır.
Şablon tetiklendiğinde, transaction_id değerini günceller.

Dönüşüm takibinin yapılabilmesi için `Influio Conversion Tracking`  ve `Influio Transaction Id Handler` şablonlarının her ikisini de yüklemelisiniz.

Template (.tpl) Dosyalarını aşağıdaki linklerden indirebilirsiniz.

[Influio Transaction Handler](https://drive.google.com/file/d/1hQAE-1dFRF9n8c8y5Dt5oIuB0CqG5YdS/view?usp=sharing)


## Kurulum

1. `Influio Transaction Handler.tpl` dosyasını [indirin](https://drive.google.com/file/d/1hQAE-1dFRF9n8c8y5Dt5oIuB0CqG5YdS/view?usp=sharing). 

2. Şablonlar sayfasından, yeni bir etiket şablonu oluşturun.

    ![tag_template](./img/tag_template.png)

3. `Influio Transaction Handler.tpl` şablonunu içeri aktarın 

    ![import_template](./img/import_template.png)
    

## Yapılandırma

Etiketler Sayfasından `Influio TransactionId Handler`'ı kullanarak yeni bir etiket oluşturun.

![create_tag](./img/transaction_create_tag.png)

Influio Dönüşüm Takip Şablonu **All Pages / Tüm Sayfalar** sayfalarda trigger edilmelidir.


Transaction Handler tag'ini oluştururken `Cookie Time` alanınını Influio tarafından iletilen değer ile doldurun.

![import_template](./img/th_create_tag.png)

`Cookie Time` : Entegrasyon esnasında tarafınıza iletilecektir. Numerik değer. Örneğin 30




### Notlar

`Cookie Time` firmanız ile mutabık kalınan çerez süresini ifade eder. Entegrasyon esnasında tarafınıza iletilecektir.

Yapılandırılmış şablon örneği
  
![template_done](./img/done.png)

Örnek kod ve script'ler geliştirme süreçlerini daha detaylı açıklamak için kullanılmıştır. Örnek kodlarda güvenlik açıkları ve buglar bulunabilir. Bu kodların kullanılması Influio tarafından kesinlikle tavsiye edilmez.
Web sayfanızda kullandığınız script içeriklerini güvenlik politikanız doğrultusunda geliştiriniz.